package co.uk.zoopla.commons;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class BrowsersManager extends DriverManager
{
    private WebDriver initialiseChrome()
    {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }

    private WebDriver initHeadlessChrome()
    {
        WebDriverManager.chromedriver().setup();

        //instantiate an object of ChromeOptions
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-gpu", "--headless");

        return new ChromeDriver(options);

    }

    private WebDriver initFirefox()
    {
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }

    private WebDriver initHeadlessFirefox()
    {
        WebDriverManager.firefoxdriver().setup();

        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--disable-gpu", "--headless");

        return new FirefoxDriver(options);
    }

    private WebDriver initInternetExplorer()
    {
        WebDriverManager.iedriver().setup();

        return new InternetExplorerDriver();
    }

    public void launchBrowser(String browser)
    {
        switch(browser)
        {
            case "Chrome":
            case "chrome":
            case "CHROME":
                driver = initialiseChrome();
                break;
            case "Firefox":
            case "firefox":
            case "FIREFOX":
                driver = initFirefox();
                break;
            case "HeadlessChrome":
            case "headlesschrome":
            case "HEADLESSCHROME":
                driver = initHeadlessChrome();
                break;
            case "HeadlessFirefox":
            case "headlessfirefox":
            case "HEADLESSFIREFOX":
                driver = initHeadlessFirefox();
                break;
            case "InternetExplorer":
            case "ie":
            case "internetexplorer":
                driver = initInternetExplorer();
                break;
            default:
                driver = initHeadlessChrome();
                break;

        }

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    }

    public void closeBrowser()
    {
        driver.manage().deleteAllCookies();
        driver.quit();

    }
}
