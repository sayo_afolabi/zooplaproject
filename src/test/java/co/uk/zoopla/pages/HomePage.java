package co.uk.zoopla.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

    //constructor
    public HomePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "search-input-location")
    private WebElement searchField;

    //@FindBy(css = "[data-responsibility=\"acceptAll\"]")
    @FindBy(css = ".ui-button-primary.ui-cookie-accept-all-medium-large")
    private WebElement acceptCookies;

    @FindBy(name = "price_min")
    private WebElement minPrice;

    @FindBy(css = ".button.button--primary")
    private WebElement searchButton;


    public void enterLocation(String location)
    {

        clickOnAcceptCookies();

        searchField.sendKeys(location);
        searchField.click();
    }

    private void clickOnAcceptCookies()
    {
        acceptCookies.click();
    }

    public void selectMinimumPrice(String miniPrice)
    {
        selectElementByText(minPrice, miniPrice);
    }

    public SearchResultPage clickOnSearchButton()
    {
        searchButton.click();
        return new SearchResultPage(driver);
    }

}
