Feature: Customer Can Search for Properties for Sale


Scenario Outline: Customer can search for properties
  Given I navigate to zoopla homepage
  When I enter my location as "<Location>"
  And I select "<MinPrice>" as my minimum price
  And I select "<MaxPrice>" as my maximum price
  And I select "<Property>" from property type
  And I select "<Bedroom>" from bedroom type
  And I click on Search button
  And the list of results are displayed
  And I click on one of the results
  Then the details of the property selected is displayed

Examples:
  | Location |MinPrice| MaxPrice | Property|Bedroom|
  | Manchester| £120,000|£300,000| Houses  | 3+    |
 # | Liverpool | £100,000| £200,000| Houses | 2+    |
 # | London    |£300,000 | £500,000| Flats  | 3+    |
 # | Coventry | £250,000 | £300,000| Houses | 3+    |

@wip
  Scenario Outline: Customer can search for properties for sale
    Given I navigate to zoopla homepage
    When I enter my location as "<Location>"
    And I select "<MinPrice>" as my minimum price
    And I select "<MaxPrice>" as my maximum price
    And I select "<Property>" from property type
    And I select "<Bedroom>" from bedroom type
    And I click on Search button
    And the list of results are displayed
    And I click on one of the results
    Then the details of the property selected is displayed

    Examples:
      | Location |MinPrice| MaxPrice | Property|Bedroom|
      | Liverpool| £120,000|£300,000| Houses  | 3+    |