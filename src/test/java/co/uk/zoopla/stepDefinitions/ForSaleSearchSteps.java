package co.uk.zoopla.stepDefinitions;

import co.uk.zoopla.pages.BasePage;
import co.uk.zoopla.pages.HomePage;
import co.uk.zoopla.pages.SearchResultPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.support.PageFactory;

public class ForSaleSearchSteps extends BasePage {


    HomePage homePage = PageFactory.initElements(driver, HomePage.class);
    SearchResultPage searchResultPage = PageFactory.initElements(driver, SearchResultPage.class);

    @Given("I navigate to zoopla homepage")
    public void i_navigate_to_zoopla_homepage() {
        launchUrl();
    }

    @When("I enter my location as {string}")
    public void i_enter_my_location_as(String location) {
        homePage.enterLocation(location);
    }

    @When("I select {string} as my minimum price")
    public void i_select_as_my_minimum_price(String miniPrice) {
        homePage.selectMinimumPrice(miniPrice);
    }

    @When("I select {string} as my maximum price")
    public void i_select_as_my_maximum_price(String string) {

    }

    @When("I select {string} from property type")
    public void i_select_from_property_type(String string) {

    }

    @When("I select {string} from bedroom type")
    public void i_select_from_bedroom_type(String string) {

    }

    @When("I click on Search button")
    public void i_click_on_search_button() {
        searchResultPage = homePage.clickOnSearchButton();
    }

    @When("the list of results are displayed")
    public void the_list_of_results_are_displayed() {

    }

    @When("I click on one of the results")
    public void i_click_on_one_of_the_results() {

    }

    @Then("the details of the property selected is displayed")
    public void the_details_of_the_property_selected_is_displayed() {

    }

}
